# Teacode recruitment task - contact app

## Description

Simple contact application.

1. Application show 25 results at once from contacts
2. You can use search to filter the whole contacts data
3. You can select contact to toggle active - results in console logging the ID of all selected
4. Contacts are sorted by last name of alphabetically

## Installation

1. Download
2. In terminal cd to 'teacode-task' folder then type npm install
3. Type npm run to open in browser
