import React, { useState, useEffect } from "react";
import Checkbox from "@material-ui/core/Checkbox";
import avatar_pic from "../img/avatar_pic.jpg";
import styled from "styled-components";

const Contact = ({
  avatar,
  firstName,
  lastName,
  email,
  id,
  active,
  users,
  setUsers,
}) => {
  const [isActive, setIsActive] = useState(active);

  //Setting the active to a contact in users array
  /* eslint-disable */
  useEffect(() => {
    active = isActive;
    let toggledUser = users.find((user) => user.id === id);
    if (toggledUser) {
      toggledUser.active = isActive;
      setUsers(
        users.filter((user) => {
          if (user.id === id) {
            return toggledUser;
          } else {
            return user;
          }
        })
      );
    }
  }, [isActive]);
  /* eslint-disable */

  // Toggling active
  const activeHandler = () => {
    setIsActive(!isActive);
    // Loop over users for ID, seting timeout to finish the state updateing
    setTimeout(() => idLoop(), 100);
  };

  // Loop over users for ID
  const idLoop = () => {
    users.forEach((user) => (user.active ? console.log(user.id) : ""));
  };

  return (
    <StyledContact
      onClick={activeHandler}
      style={active ? { background: "#71a3ff" } : {}}>
      <img src={avatar ? avatar : avatar_pic} alt={`${lastName}`}></img>
      <div className="container-text">
        <h4>
          {firstName} {lastName}
        </h4>
        <p>{email}</p>
      </div>
      <Checkbox
        className="checkbox"
        checked={active ? true : false}
        color="primary"
        inputProps={{ "aria-label": "secondary checkbox" }}
      />
    </StyledContact>
  );
};
const StyledContact = styled.div`
  padding: 1rem;
  display: flex;
  align-items: center;
  border-radius: 0.5rem;
  background: #e9e9e9;
  box-shadow: 2px 2px 15px -3px rgba(0, 0, 0, 0.5);
  margin: 0.5rem 0;
  width: 100%;
  cursor: pointer;
  transition: 0.5s ease;
  img {
    width: 60px;
    height: 60px;
    object-fit: cover;
    border-radius: 50%;
    box-shadow: 2px 2px 15px -3px rgba(0, 0, 0, 0.5);
  }
  .container-text {
    width: 100%;
    margin-left: 1rem;
  }
  .checkbox {
    justify-self: flex-end;
  }

  &:hover {
    background: white;
  }
  @media (max-width: 400px) {
    font-size: 75%;
    padding: 0.2rem;
    .container-text {
      width: 100%;
      margin-left: 0.3rem;
    }
  }
`;

export default Contact;
