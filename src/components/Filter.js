import React from "react";
import styled from "styled-components";

const Filter = ({ filterInput, setFilterInput }) => {
  const filterInputHandler = (e) => {
    setFilterInput(e.target.value);
  };
  return (
    <StyledFilter>
      <i className="fas fa-search fa-2x" />
      <input
        value={filterInput}
        onChange={filterInputHandler}
        placeholder="Search..."
        type="text"
      />
    </StyledFilter>
  );
};

const StyledFilter = styled.div`
  margin: 0.5rem 0;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 80%;
  input {
    box-shadow: 2px 2px 15px -3px rgba(0, 0, 0, 0.5);
    border: none;
    border-radius: 5px;
    width: 100%;
    height: 2.5rem;
    padding: 0.5rem;
    font-size: 1.2rem;
  }
  i {
    margin-right: 0.5rem;
  }
`;

export default Filter;
