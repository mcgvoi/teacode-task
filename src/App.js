import React, { useEffect, useState } from "react";
// Global style
import GlobalStyle from "./GlobalStyle";
// Components
import Filter from "./components/Filter";
import Contact from "./components/Contact";
// Styled components
import styled from "styled-components";

const App = () => {
  useEffect(() => {
    getData();
  }, []);

  const [users, setUsers] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [filterInput, setFilterInput] = useState("");
  const [filterResults, setFilterResults] = useState([]);

  // Fetching tha data
  const getData = async () => {
    const response = await fetch(
      "https://dry-taiga-21390.herokuapp.com/https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
    );
    const data = await response.json();
    // Setting state with the data, sorting by last_name, adding active prop to objects, to track if they are active
    setUsers(
      data
        .sort((a, b) => a.last_name.localeCompare(b.last_name))
        .map((user) => ({ ...user, active: false }))
    );
  };
  // Populating search results array// eslint-disable-line react-hooks/exhaustive-deps
  /* eslint-disable */
  useEffect(() => (users.length ? setIsLoaded(true) : ""), [users]);
  useEffect(() => (isLoaded ? setFilterResults(users.slice(0, 15)) : ""), [
    isLoaded,
  ]);
  /* eslint-disable */

  // Filtering by first and last name
  useEffect(() => {
    const results = users.filter((user) => {
      const name = `${user.first_name} ${user.last_name}`;
      return name.toLocaleLowerCase().includes(filterInput.toLocaleLowerCase());
    });
    setFilterResults(results.slice(0, 15));
  }, [filterInput]);

  return (
    <div className="App">
      {isLoaded && (
        <Home>
          <GlobalStyle />
          <StyledHeader>
            <h1>Contacts</h1>
          </StyledHeader>
          <Filter filterInput={filterInput} setFilterInput={setFilterInput} />
          <ul className="contact-list">
            {filterResults.map((user) => (
              <Contact
                key={user.id}
                avatar={user.avatar}
                firstName={user.first_name}
                lastName={user.last_name}
                email={user.email}
                id={user.id}
                active={user.active}
                users={users}
                setUsers={setUsers}
              />
            ))}
          </ul>
        </Home>
      )}
    </div>
  );
};
const Home = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  ul {
    width: 100%;
  }
`;
const StyledHeader = styled.div`
  margin: 0.5rem 0;
  text-align: center;
`;

export default App;
